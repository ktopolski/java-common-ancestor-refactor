/**
 * Common Ancestor for RefactoredProcesor and RefactoredController.
 * @author Karol Topolski
 */
public class CommonAncestor {
    public String View(){ return "View"; }
    public String doDraw(){ return "doDraw"; }
    public String doDelete(){ return "doDelete"; }
}
