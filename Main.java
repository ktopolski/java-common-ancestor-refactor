/**
 * Main class for testing if refactor really works.
 * @author Karol Topolski
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Procesor procesor = new Procesor();
        Controller controller = new Controller();
        RefactoredController rController = new RefactoredController();
        RefactoredProcesor rProcesor = new RefactoredProcesor();
        boolean refactorCorrect = procesor.View().equals(rProcesor.View()) &&
                procesor.doDraw().equals(rProcesor.doDraw())               &&
                procesor.doDelete().equals(rProcesor.doDelete())           &&
                controller.View().equals(rController.View())               &&
                controller.doDraw().equals(rController.doDraw())           &&
                controller.doDelete().equals(rController.doDelete());
        System.out.println("Refactor succeded? " + refactorCorrect);
    }
    
}
