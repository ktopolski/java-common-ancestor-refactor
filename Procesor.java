/**
 * First class with duplicated methods.
 * @author Karol Topolski
 */
public class Procesor {
    public String View(){ return "View"; }
    public String doDraw(){ return "doDraw"; }
    public String doDelete(){ return "doDelete"; }
}
